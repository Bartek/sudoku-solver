/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zabek;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author zabek
 */
public class SudokuSolverTest {
    
    public SudokuSolverTest() {
    }

    @Test
    public void testConvertTextToFields() {
        SudokuSolver solver = new SudokuSolver(new SudokuField[1][1]);
        int row = 1;
        SudokuField[] result = solver.convertTextToFields("12345678_", row);
        SudokuField[] expectedResult = new SudokuField[9];
        for (int i = 0; i < 8; i++) {
            expectedResult[i] = new SudokuField(row, i, i + 1);
        }
        expectedResult[8] = new SudokuField(row, 8, null);
        assertArrayEquals(expectedResult, result);
    }
    
//    @Test
    public void test(){
        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
        String row = solver.readRow(getReader("4982 31 7"));
        solver.sudoku[0] = solver.convertTextToFields(row, 0);
        
        row = solver.readRow(getReader("   5 82 4"));
        solver.sudoku[1] = solver.convertTextToFields(row, 1);
        
        row = solver.readRow(getReader(" 7 49 6  "));
        solver.sudoku[2] = solver.convertTextToFields(row, 2);
        
        row = solver.readRow(getReader("8   4  65"));
        solver.sudoku[3] = solver.convertTextToFields(row, 3);
        
        row = solver.readRow(getReader("6 3      "));
        solver.sudoku[4] = solver.convertTextToFields(row, 4);
        
        row = solver.readRow(getReader("2 7 59 31"));
        solver.sudoku[5] = solver.convertTextToFields(row, 5);
        
        row = solver.readRow(getReader(" 6  3  2 "));
        solver.sudoku[6] = solver.convertTextToFields(row, 6);
        
        row = solver.readRow(getReader("98 1  3  "));
        solver.sudoku[7] = solver.convertTextToFields(row, 7);
        
        row = solver.readRow(getReader(" 2478 519"));
        solver.sudoku[8] = solver.convertTextToFields(row, 8);
        
        solver.drawSudoku();
        
        solver.initPotentialNumbers();
        solver.findSingleValues();
//        solver.drawSudoku();
        
    }
    
    BufferedReader getReader(String text){
        return new BufferedReader(new InputStreamReader(new ByteArrayInputStream(text.getBytes())));
    }
    
//    @Test
    public void testSolverEasy(){
        System.out.println("Easy test");
        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
        String row = solver.readRow(getReader("  6 2 7  "));
        solver.sudoku[0] = solver.convertTextToFields(row, 0);
        
        row = solver.readRow(getReader("9  8 7  5"));
        solver.sudoku[1] = solver.convertTextToFields(row, 1);
        
        row = solver.readRow(getReader(" 1  5  4 "));
        solver.sudoku[2] = solver.convertTextToFields(row, 2);
        
        row = solver.readRow(getReader("25     84"));
        solver.sudoku[3] = solver.convertTextToFields(row, 3);
        
        row = solver.readRow(getReader(" 38   92 "));
        solver.sudoku[4] = solver.convertTextToFields(row, 4);
        
        row = solver.readRow(getReader("17     63"));
        solver.sudoku[5] = solver.convertTextToFields(row, 5);
        
        row = solver.readRow(getReader(" 2  7  9 "));
        solver.sudoku[6] = solver.convertTextToFields(row, 6);
        
        row = solver.readRow(getReader("4  1 5  8"));
        solver.sudoku[7] = solver.convertTextToFields(row, 7);
        
        row = solver.readRow(getReader("  1 3 4  "));
        solver.sudoku[8] = solver.convertTextToFields(row, 8);
        
        solver.drawSudoku();
        
        solver.initPotentialNumbers();
        solver.findSingleValues();
        System.out.println("Easy test end");
    }
    
//    @Test
    public void testSolverMedium(){
        System.out.println("Medium test");
        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
        String row = solver.readRow(getReader("        8"));
        solver.sudoku[0] = solver.convertTextToFields(row, 0);
        
        row = solver.readRow(getReader("  8 5 9 2"));
        solver.sudoku[1] = solver.convertTextToFields(row, 1);
        
        row = solver.readRow(getReader("  681    "));
        solver.sudoku[2] = solver.convertTextToFields(row, 2);
        
        row = solver.readRow(getReader("41   9 5 "));
        solver.sudoku[3] = solver.convertTextToFields(row, 3);
        
        row = solver.readRow(getReader(" 6     3 "));
        solver.sudoku[4] = solver.convertTextToFields(row, 4);
        
        row = solver.readRow(getReader(" 3 1   97"));
        solver.sudoku[5] = solver.convertTextToFields(row, 5);
        
        row = solver.readRow(getReader("    735  "));
        solver.sudoku[6] = solver.convertTextToFields(row, 6);
        
        row = solver.readRow(getReader("6 9 2 1  "));
        solver.sudoku[7] = solver.convertTextToFields(row, 7);
        
        row = solver.readRow(getReader("3        "));
        solver.sudoku[8] = solver.convertTextToFields(row, 8);
        
        solver.drawSudoku();
        
        solver.initPotentialNumbers();
        solver.findSingleValues();
        System.out.println("Medium test end");
    }
    
    @Test
    public void testSolverHard(){
        System.out.println("Hard test");
        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
        String row = solver.readRow(getReader("  43 72  "));
        solver.sudoku[0] = solver.convertTextToFields(row, 0);
        
        row = solver.readRow(getReader(" 2  4  5 "));
        solver.sudoku[1] = solver.convertTextToFields(row, 1);
        
        row = solver.readRow(getReader("7       9"));
        solver.sudoku[2] = solver.convertTextToFields(row, 2);
        
        row = solver.readRow(getReader(" 1 6 3 7 "));
        solver.sudoku[3] = solver.convertTextToFields(row, 3);
        
        row = solver.readRow(getReader("         "));
        solver.sudoku[4] = solver.convertTextToFields(row, 4);
        
        row = solver.readRow(getReader(" 6 8 2 3 "));
        solver.sudoku[5] = solver.convertTextToFields(row, 5);
        
        row = solver.readRow(getReader("3       7"));
        solver.sudoku[6] = solver.convertTextToFields(row, 6);
        
        row = solver.readRow(getReader(" 5  8  6 "));
        solver.sudoku[7] = solver.convertTextToFields(row, 7);
        
        row = solver.readRow(getReader("  87 51  "));
        solver.sudoku[8] = solver.convertTextToFields(row, 8);
        
        solver.drawSudoku();
        
        solver.initPotentialNumbers();
        solver.findSingleValues();
        System.out.println("Hard test end");
    }
    
    @Test
    public void testSolverHard2(){
        System.out.println("Hard test 2");
        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
        String row = solver.readRow(getReader("9  7     "));
        solver.sudoku[0] = solver.convertTextToFields(row, 0);
        
        row = solver.readRow(getReader(" 6  2 3  "));
        solver.sudoku[1] = solver.convertTextToFields(row, 1);
        
        row = solver.readRow(getReader("   956 4 "));
        solver.sudoku[2] = solver.convertTextToFields(row, 2);
        
        row = solver.readRow(getReader(" 21   8  "));
        solver.sudoku[3] = solver.convertTextToFields(row, 3);
        
        row = solver.readRow(getReader("8       9"));
        solver.sudoku[4] = solver.convertTextToFields(row, 4);
        
        row = solver.readRow(getReader("  9   62 "));
        solver.sudoku[5] = solver.convertTextToFields(row, 5);
        
        row = solver.readRow(getReader(" 1 574   "));
        solver.sudoku[6] = solver.convertTextToFields(row, 6);
        
        row = solver.readRow(getReader("  5 6  7 "));
        solver.sudoku[7] = solver.convertTextToFields(row, 7);
        
        row = solver.readRow(getReader("     3  4"));
        solver.sudoku[8] = solver.convertTextToFields(row, 8);
        
        solver.drawSudoku();
        
        solver.initPotentialNumbers();
        solver.findSingleValues();
        System.out.println("Hard test 2 end");
    }
    
    @Test
    public void testSolverVeryHard(){
        System.out.println("Very hard test");
        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
        String row = solver.readRow(getReader("2 8  51  "));
        solver.sudoku[0] = solver.convertTextToFields(row, 0);
        
        row = solver.readRow(getReader("  9     5"));
        solver.sudoku[1] = solver.convertTextToFields(row, 1);
        
        row = solver.readRow(getReader("6        "));
        solver.sudoku[2] = solver.convertTextToFields(row, 2);
        
        row = solver.readRow(getReader("  58 4 67"));
        solver.sudoku[3] = solver.convertTextToFields(row, 3);
        
        row = solver.readRow(getReader("   7 1   "));
        solver.sudoku[4] = solver.convertTextToFields(row, 4);
        
        row = solver.readRow(getReader("74 5 62  "));
        solver.sudoku[5] = solver.convertTextToFields(row, 5);
        
        row = solver.readRow(getReader("        3"));
        solver.sudoku[6] = solver.convertTextToFields(row, 6);
        
        row = solver.readRow(getReader("4     7  "));
        solver.sudoku[7] = solver.convertTextToFields(row, 7);
        
        row = solver.readRow(getReader("  63  4 2"));
        solver.sudoku[8] = solver.convertTextToFields(row, 8);
        
        solver.drawSudoku();
        
        solver.initPotentialNumbers();
        solver.findSingleValues();
        System.out.println("Very hard test end");
    }
    
    @Test
    public void testSolverVeryHard2(){
        System.out.println("Very hard test 2");
        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
        String row = solver.readRow(getReader("5 8      "));
        solver.sudoku[0] = solver.convertTextToFields(row, 0);
        
        row = solver.readRow(getReader(" 24 785  "));
        solver.sudoku[1] = solver.convertTextToFields(row, 1);
        
        row = solver.readRow(getReader(" 7      1"));
        solver.sudoku[2] = solver.convertTextToFields(row, 2);
        
        row = solver.readRow(getReader("   2 3   "));
        solver.sudoku[3] = solver.convertTextToFields(row, 3);
        
        row = solver.readRow(getReader("1   6    "));
        solver.sudoku[4] = solver.convertTextToFields(row, 4);
        
        row = solver.readRow(getReader("      4  "));
        solver.sudoku[5] = solver.convertTextToFields(row, 5);
        
        row = solver.readRow(getReader("   5   96"));
        solver.sudoku[6] = solver.convertTextToFields(row, 6);
        
        row = solver.readRow(getReader("  2 36  5"));
        solver.sudoku[7] = solver.convertTextToFields(row, 7);
        
        row = solver.readRow(getReader("4 5    78"));
        solver.sudoku[8] = solver.convertTextToFields(row, 8);
        
        solver.drawSudoku();
        
        solver.initPotentialNumbers();
        solver.findSingleValues();
        System.out.println("Very hard test2 end");
    }
    
    //Najtrudnieszje nie zrobione
//    @Test
//    public void testSolverVeryHard3(){
//        System.out.println("Very hard test 3");
//        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
//        String row = solver.readRow(getReader("8        "));
//        solver.sudoku[0] = solver.convertTextToFields(row, 0);
//        
//        row = solver.readRow(getReader("  36     "));
//        solver.sudoku[1] = solver.convertTextToFields(row, 1);
//        
//        row = solver.readRow(getReader(" 7  9 2  "));
//        solver.sudoku[2] = solver.convertTextToFields(row, 2);
//        
//        row = solver.readRow(getReader(" 5   7   "));
//        solver.sudoku[3] = solver.convertTextToFields(row, 3);
//        
//        row = solver.readRow(getReader("    457  "));
//        solver.sudoku[4] = solver.convertTextToFields(row, 4);
//        
//        row = solver.readRow(getReader("   1   3 "));
//        solver.sudoku[5] = solver.convertTextToFields(row, 5);
//        
//        row = solver.readRow(getReader("  1    68"));
//        solver.sudoku[6] = solver.convertTextToFields(row, 6);
//        
//        row = solver.readRow(getReader("  85   1 "));
//        solver.sudoku[7] = solver.convertTextToFields(row, 7);
//        
//        row = solver.readRow(getReader(" 9    4  "));
//        solver.sudoku[8] = solver.convertTextToFields(row, 8);
//        
//        solver.drawSudoku();
//        
//        solver.initPotentialNumbers();
//        solver.findSingleValues();
//        System.out.println("Very hard test3 end");
//    }
    
//    @Test
    public void testPatterns(){
        Map<Integer, List<SudokuField>> numberPotentialPlaces = new HashMap<>();
        SudokuField f1 = new SudokuField(1, 1, null);
        f1.setPotentialNumbers(new ArrayList<>(Arrays.asList(1, 2, 3)));
        
        SudokuField f2 = new SudokuField(2, 2, null);
        f2.setPotentialNumbers(new ArrayList<>(Arrays.asList(1, 2, 3)));
        
        SudokuField f3 = new SudokuField(3, 3, null);
        f3.setPotentialNumbers(new ArrayList<>(Arrays.asList(3)));
        
        numberPotentialPlaces.put(1, new ArrayList<>(Arrays.asList(f1, f2, f3)));
        numberPotentialPlaces.put(2, new ArrayList<>(Arrays.asList(f1, f2)));
        numberPotentialPlaces.put(3, new ArrayList<>(Arrays.asList(f1, f2)));
        
        SudokuSolver solver = new SudokuSolver(new SudokuField[9][9]);
        solver.findPatterns(numberPotentialPlaces);
        
        assertEquals(1, f1.getPotentialNumbers().size());
    }
}
