/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zabek;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author zabek
 */
public class SudokuSolver {
    private final Set<String> correctSigns = Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", " ", "_").stream().collect(Collectors.toSet());
    final SudokuField[][] sudoku;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new SudokuSolver();
    }

    public SudokuSolver(SudokuField[][] sudoku) {
        this.sudoku = sudoku;
    }

    public SudokuSolver() {
        sudoku = new SudokuField[9][9];
        initSudoku();
        findSingleValues();
    }
    
    
    private void initSudoku(){
        for (int i = 0; i < 9; i++) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Wprowadź " + (i + 1) + " wiersz\n");
            String row = readRow(br);
            sudoku[i] = convertTextToFields(row, i);
        }
        
        initPotentialNumbers();
    }
    
    String readRow(BufferedReader br) {
        String row = null;
        
        while (row == null) {
            try {
                String potentialRow = br.readLine();
                boolean isCorrect = true;
                if(potentialRow.length() != 9){
                    System.out.println("Na wiersz danych musi się składać 9 znaków, wprowadzono " + potentialRow.length() + ". Spróbuj ponownie");
                    isCorrect = false;
                }
                
                for (char sign : potentialRow.toCharArray()) {
                    if(!correctSigns.contains(String.valueOf(sign))){
                        System.out.println("Wprowadzono zły znak: " + sign);
                        isCorrect = false;
                    }
                }
                
                if(isCorrect)
                    row = potentialRow;
            } catch (IOException ex) {
                Logger.getLogger(SudokuSolver.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return row;
    }
    
    SudokuField[] convertTextToFields(String text, int row){
        SudokuField[] fields = new SudokuField[9];
        int i = 0;
        for (char c : text.toCharArray()) {
            switch(c){
                case ' ':
                case '_':
                    fields[i] = new SudokuField(row, i, null);
                    break;
                default:
                    fields[i] = new SudokuField(row, i, Character.getNumericValue(c));
            }
            
            i++;
        }
        
        return fields;
    } 
    
    void initPotentialNumbers(){
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if(sudoku[i][j].getFinalNumber() != null){
                    removePotentialFromColumnRowAndBlock(i, j, sudoku[i][j].getFinalNumber());
                }
            }
        }
    }
    
    void removePotentialFromColumnRowAndBlock(int row, int column, Integer numb){
        Arrays.stream(sudoku).forEach(r->r[column].getPotentialNumbers().remove(numb));
        Arrays.stream(sudoku[row]).forEach(r->r.getPotentialNumbers().remove(numb));
        
        int startX = getBlockFirsIndex(row);
        int startY = getBlockFirsIndex(column);
        for (int i = startX; i < startX + 3; i++) {
            for (int j = startY; j < startY + 3; j++) {
                sudoku[i][j].getPotentialNumbers().remove(numb);
            }
        }
    }
    
    void findSingleValues() {
        int changedRows = -1;drawSudoku();
        while (changedRows != 0) {
            changedRows = 0;
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (sudoku[i][j].getPotentialNumbers().size() == 1) {
                        sudoku[i][j].setFinalNumber(sudoku[i][j].getPotentialNumbers().iterator().next());
                        removePotentialFromColumnRowAndBlock(i, j, sudoku[i][j].getFinalNumber());
                        changedRows++;
                    }
                }
                
                changedRows += fillSingleMissingNumb(sudoku[i]);
                final int ii = i;
                changedRows += fillSingleMissingNumb(Arrays.stream(sudoku).map(s->s[ii]).toArray(size -> new SudokuField[size]));
            }
            
            changedRows += fillSinglePotentialBlocksNumb();
            
            if(changedRows != 0){
                System.out.println("rows changed " + changedRows);
                drawSudoku();
            }
        }
    }
     
    int fillSingleMissingNumb(SudokuField[] table) {
        List<Integer> rowNumbers
                = Arrays.asList(table).stream()
                        .filter(s -> s.getFinalNumber() != null)
                        .map(s -> s.getFinalNumber()).collect(Collectors.toList());
        if (rowNumbers.size() == 8) {
            Integer numbToFill = IntStream.range(1, 10).filter(r -> !rowNumbers.contains(r)).findAny().getAsInt();
            for (SudokuField sudokuField : table) {
                if (sudokuField.getFinalNumber() == null) {
                    sudokuField.setFinalNumber(numbToFill);
                    removePotentialFromColumnRowAndBlock(sudokuField.row, sudokuField.column, numbToFill);
                    break;
                }
            }
            return 1;
        }
        
        Map<Integer, List<SudokuField>> numberPotentialPlaces = numberPotentialPlaces(table);
        int changes = 0;
        
        List<Integer> keysToRemove = new ArrayList<>();
        
        for (Map.Entry<Integer, List<SudokuField>> entry : numberPotentialPlaces.entrySet()) {
            Integer key = entry.getKey();
            List<SudokuField> fields = entry.getValue();
            if(fields.size() == 0){
                keysToRemove.add(key);
                continue;
            }
            
            if(fields.size() == 1){
                SudokuField field = fields.get(0);
                
                field.setFinalNumber(key);
                
                removePotentialFromColumnRowAndBlock(field.row, field.column, key);
                changes ++;
            }
        }
        
        keysToRemove.forEach(k->numberPotentialPlaces.remove(k));
        
        if(numberPotentialPlaces.size() > 1){
            changes += findPatterns(numberPotentialPlaces);
        }
        
        return changes;
    }
    
    Map<Integer, List<SudokuField>> numberPotentialPlaces(SudokuField[] table){
        Map<Integer, List<SudokuField>> numberPotentialPlaces
                = new HashMap<>(IntStream.range(1, 10).boxed()
                        .collect(Collectors.toMap(i -> i, i -> new ArrayList<>())));
        
        int changes = 0;
        
        for (SudokuField sudokuField : table) {
            if(sudokuField.getFinalNumber() == null){
                for (Integer potentialNumber : sudokuField.getPotentialNumbers()) {
                    numberPotentialPlaces.get(potentialNumber).add(sudokuField);
                }
            }
        }
        
        return numberPotentialPlaces;
    }
    
    int findPatterns(Map<Integer, List<SudokuField>> numberPotentialPlaces){
        int change = 0;
        for (Map.Entry<Integer, List<SudokuField>> entry : numberPotentialPlaces.entrySet()) {
            Integer firstKey = entry.getKey();
            List<SudokuField> firstValues = entry.getValue();
            
            for (Map.Entry<Integer, List<SudokuField>> entry1 : numberPotentialPlaces.entrySet()) {
                Integer secondKey = entry1.getKey();
                List<SudokuField> secondValues = entry1.getValue();
                
                if(firstKey != secondKey && firstValues.size() == 2 && secondValues.size() == 2 && firstValues.containsAll(secondValues)){
                    for (SudokuField secondValue : secondValues) {
                        if(secondValue.getPotentialNumbers().size() > 2){
                            secondValue.setPotentialNumbers(new ArrayList<>(Arrays.asList(firstKey, secondKey)));
                            change = 1;
                        }
                    }
                }
            }
            
        }
        return change;
    }
    
    int removePotentialFromRowExceptBlock(Integer numbToRemove, int blockFirstNumb, int row){
        int change = 0;
        
        for (int i = 0; i < 9; i++) {
            if(i > blockFirstNumb + 2 || i <blockFirstNumb){
                if(sudoku[row][i].getPotentialNumbers().contains(numbToRemove)){
                    change = 1;
                    sudoku[row][i].getPotentialNumbers().remove(numbToRemove);
                }
            }
        }
        
        return change;
    }
    
    int removePotentialFromColumnExceptBlock(Integer numbToRemove, int blockFirstNumb, int column){
        int change = 0;
        
        for (int i = 0; i < 9; i++) {
            if(i > blockFirstNumb + 2 || i < blockFirstNumb){
                if(sudoku[i][column].getPotentialNumbers().contains(numbToRemove)){
                    sudoku[i][column].getPotentialNumbers().remove(numbToRemove);
                    change += 1;
                }
                
            }
        }
        
        return change;
    }
    
    int fillSinglePotentialBlocksNumb(){
        int changes = 0;
        for (int i = 0; i < 8; i+= 3) {
            for (int j = 0; j < 8; j+=3) {
                ArrayList<SudokuField> fieldList = new ArrayList<>();
                fieldList.addAll(Arrays.asList(Arrays.copyOfRange(sudoku[i], j, j + 3)));
                fieldList.addAll(Arrays.asList(Arrays.copyOfRange(sudoku[i + 1], j, j + 3)));
                fieldList.addAll(Arrays.asList(Arrays.copyOfRange(sudoku[i + 2], j, j + 3)));
                
                SudokuField[] array = fieldList.toArray(new SudokuField[0]);
                changes += fillSingleMissingNumb(array);
                
                changes += findSameRowColumnBlockPatterns(array);
            }
        }
        
        return changes;
    }
    
    int findSameRowColumnBlockPatterns(SudokuField[] array){
        int change = 0;
        Map<Integer, List<SudokuField>> numberPotentialPlaces = numberPotentialPlaces(array);
        for (Map.Entry<Integer, List<SudokuField>> entry : numberPotentialPlaces.entrySet()) {
            Integer key = entry.getKey();
            List<SudokuField> values = entry.getValue();
            if(values.size() <2)
                continue;

            //wszystkie potencjalne miejsca znajdują się w tym samym wierszu
            if (values.stream().map(v -> v.row).distinct().count() == 1) {
                change += removePotentialFromRowExceptBlock(key, getBlockFirsIndex(values.get(0).column), values.get(0).row);
            }
            
            if (values.stream().map(v -> v.column).distinct().count() == 1) {
                change += removePotentialFromColumnExceptBlock(key, getBlockFirsIndex(values.get(0).row), values.get(0).column);
            }
        }
        
        return change;
    }
    
    void drawSudoku() {
        System.out.println("\n");

        for (int i = 0; i < sudoku.length; i++) {
            String row = "";
            if (i % 3 == 0) {
                row = "\n";
            }
            for (int j = 0; j < sudoku[0].length; j++) {
                if ((j) % 3 == 0) {
                    row += " ";
                }
                row += sudoku[i][j].getFinalNumberString();
                if(j != 8){
                    row += "|";
                }
            }
            System.out.println(row);
        }

    }
    
    public int getBlockFirsIndex(int currentIndex){
        return (currentIndex / 6 == 1) ? 6 : (currentIndex / 3 == 1) ? 3 : 0;
    }
}
