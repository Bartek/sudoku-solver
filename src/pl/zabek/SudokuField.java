/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.zabek;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author zabek
 */
public class SudokuField {
    final int row;
    final int column;
    List<Integer> potentialNumbers;
    Integer finalNumber;

    public SudokuField(int row, int column, Integer finalNumber) {
        this.row = row;
        this.column = column;
        this.finalNumber = finalNumber;
        
        if(finalNumber == null){
            potentialNumbers = IntStream.range(1, 10).boxed().collect(Collectors.toList());
        } else{
            potentialNumbers = new ArrayList<>(1);
        }
    }

    public List<Integer> getPotentialNumbers() {
        return potentialNumbers;
    }

    public void setPotentialNumbers(List<Integer> potentialNumbers) {
        this.potentialNumbers = potentialNumbers;
    }

    public Integer getFinalNumber() {
        return finalNumber;
    }

    public void setFinalNumber(Integer finalNumber) {
        if(this.finalNumber != null){
            throw new RuntimeException("Nie można zmienić wartości");
        }
        
        this.finalNumber = finalNumber;
        if(finalNumber != null){
            potentialNumbers = new ArrayList<>(1);
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.row;
        hash = 43 * hash + this.column;
        hash = 43 * hash + Objects.hashCode(this.finalNumber);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SudokuField other = (SudokuField) obj;
        if (this.row != other.row) {
            return false;
        }
        if (this.column != other.column) {
            return false;
        }
        if (!Objects.equals(this.finalNumber, other.finalNumber)) {
            return false;
        }
        return true;
    }
    
    public String getFinalNumberString(){
        if(finalNumber == null)
            return "_";
        return finalNumber.toString();
    }
}
